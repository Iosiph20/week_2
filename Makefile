#******************************************************************************
# Copyright (C) 2017 by Alex Fosdick - University of Colorado
#
# Redistribution, modification or use of this software in source or binary
# forms is permitted as long as the files maintain this copyright. Users are 
# permitted to modify this and use it to learn about the field of embedded
# software. Alex Fosdick and the University of Colorado are not liable for any
# misuse of this material. 
#
#*****************************************************************************

#------------------------------------------------------------------------------
# Simple makefile for week 2 task. It's able to compile for multiple platforms
#
# Use: make [TARGET] [PLATFORM-OVERRIDES]
#
# Build Targets:
#      <File>.o    - Build <File>.o object file
#      <File>.i    - Creates <File>.i preprocessed file
#      <File>.asm  - Creates <File>.asm assembly text file
#      build       - Builds the <TARGET>.out executable file
#      compile-all - Builds all the translation units object files
#      clean       - Cleans all the output files for a specified PLATFORM
#
# Platform Overrides:
#      TARGET   - Specifies the output executable file name
#
#      PLATFORM - Selects the target platform. Current possible values:
#
#                   HOST
#                   MSP432
#
#------------------------------------------------------------------------------
include sources.mk

# Platform Overrides
PLATFORM = HOST

# General Purpose Vars
TARGET = c1m2
LOG = build.log

# Common Compiler Flags and Defines
LDFLAGS = -Wl,-Map=$(TARGET).map
CFLAGS = -std=c99 -O0 -g -Wall

ifeq ($(PLATFORM),HOST)
  # Toolchain configuration
  CC = gcc
  LD = ld
  SIZE = size
  OBJDUMP = objdump

  #Additional Flags
  CFLAGS += -DHOST
else
  # Toolchain configuraiton
  CC = arm-none-eabi-gcc
  LD = arm-none-eabi-ld
  SIZE = arm-none-eabi-size
  OBJDUMP = arm-none-eabi-objdump

  # Additional Flags
  LDFLAGS += -T msp432p401r.lds
  CFLAGS += -DMSP432 \
            -mcpu=cortex-m4 \
            -mthumb \
            -march=armv7e-m \
            -mfloat-abi=hard \
            -mfpu=fpv4-sp-d16 \
            --specs=nosys.specs

  # Additional files
  SOURCES += src/interrupts_msp432p401r_gcc.c \
             src/startup_msp432p401r_gcc.c \
             src/system_msp432p401r.c

  INCLUDES += -I include/msp432 \
              -I include/CMSIS
endif

# Compiler Flags and Defines
CPPFLAGS = -MD

OBJS = $(SOURCES:.c=.o)
PREPROC = $(SOURCES:.c=.i)
ASSEMBLY = $(SOURCES:.c=.asm)
DEP = $(SOURCES:.c=.d)

%.o : %.c
	$(CC) $(CFLAGS) $(CPPFLAGS) $(INCLUDES) -c $< -o $@

%.i : %.c
	$(CC) -E $< $(CFLAFS) $(INCLUDES) -o $@

$(TARGET).asm : $(TARGET).out
	$(OBJDUMP) -D $< > $@

%.asm : %.c
	$(CC) $(CFLAGS) $(INCLUDES) -S $<  -o $@

$(TARGET).out : $(OBJS)
	$(CC) $(CFLAGS) $(LDFLAGS) $^ -o $@

.PHONY: build
build: $(TARGET).out
	@echo "Final build report:" | tee -a $(LOG)
	@size $(TARGET).out | tee -a $(LOG)

.PHONY: compile-all
compile-all: $(OBJS)

.PHONY: clean
clean:
	rm -f $(OBJS) $(PREPROC) $(ASSEMBLY) $(DEP) $(LOG) $(TARGET).out $(TARGET).map
